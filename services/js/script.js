function myservices($scope){
    $scope.services=[
        { title: 'Tea', price: 3},
        { title: 'Coffee', price: 2.5},
        { title: 'Bread', price: 5},
		{ title: 'Muffins', price: 4},
        { title: 'Cheese Burger', price: 5},
		{ title: 'French Fries', price: 3},
        { title: 'Cookies', price: 6},
		{ title: 'chicken Pizza', price: 10},
        { title: 'Ice-cream', price: 3},
		{ title: 'Ham Pizza', price: 12},
        { title: 'Blueberry Muffins', price: 3},
        { title: 'Kip Corn', price: 4}];
    $scope.total=function(){
        var t = 0;
        angular.forEach($scope.services, function(s){
            if(s.selected)
                t+=s.price;
        });
        return t;
    };
}